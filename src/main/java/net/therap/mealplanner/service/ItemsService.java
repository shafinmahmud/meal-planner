package net.therap.mealplanner.service;

import net.therap.mealplanner.controller.ConsoleView;
import net.therap.mealplanner.dao.ItemDao;
import net.therap.mealplanner.dto.TabularDto;
import net.therap.mealplanner.entity.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/14/16
 */
public class ItemsService {

    private ItemDao itemDao;

    public ItemsService() {
        this.itemDao = new ItemDao();
        this.itemDao.createTable();
    }

    public TabularDto getItemsTable() {
        TabularDto dto = new TabularDto();
        dto.setBanner(ConsoleView.AVAILABLE_ITEMS);
        dto.setTitle(ConsoleView.ITEMS_TITLE);

        List<String> options = new ArrayList<>();
        options.add("1. To Remove an Item insert D[id]");
        options.add("2. To Update an Item insert U[id] [new name]");
        options.add("3. To Add an Item insert I[item name]");
        options.add("4. Return Back to Home");
        options.add("0. Exit");
        dto.setOptions(options);

        dto.setCount(itemDao.countAll());
        dto.setTableHead(String.format("%15s%15s", "#id", "item"));
        dto.setTableHead(dto.getTableHead() + "\n" + ConsoleView.SEPARATION_LINE);

        List<Item> itemList = itemDao.findAll();
        List<String> tableRows = new ArrayList<>();
        for (Item item : itemList) {
            tableRows.add(String.format("%15s%15s", item.getId(), item.getName()));
        }
        tableRows.add(ConsoleView.SEPARATION_LINE);
        dto.setTableRows(tableRows);
        return dto;
    }

    public void insertItems(String items) {
        String[] itemArray = items.split(",");
        for (String item : itemArray) {
            if (itemDao.findOneByName(item) == null) {
                Item itemEntity = new Item();
                itemEntity.setName(item);
                itemDao.insert(itemEntity);
            }
        }
    }

    public void updateItem(int id, String newName) {
        Item oldItem = new Item(id);
        Item newItem = new Item();
        newItem.setName(newName);
        if (itemDao.doesExist(oldItem)) {
            if (itemDao.findOneByName(newName) == null) {
                itemDao.update(oldItem, newItem);
            }else{
                itemDao.delete(oldItem);
            }
        }
    }

    public void deleteItem(int id) {
        Item item = new Item(id);
        if (itemDao.doesExist(item)) {
            itemDao.delete(item);
        }
    }
}

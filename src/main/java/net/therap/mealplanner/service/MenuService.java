package net.therap.mealplanner.service;

import net.therap.mealplanner.controller.ConsoleView;
import net.therap.mealplanner.dao.*;
import net.therap.mealplanner.dto.TabularDto;
import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import net.therap.mealplanner.entity.MealType;
import net.therap.mealplanner.entity.WeekDay;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/2016
 */
public class MenuService {

    private ItemDao itemDao;
    private MealTypeDao mealTypesDao;
    private MealDao mealTimesDao;
    private MealItemDao mealMenuDao;

    public MenuService() {
        this.itemDao = new ItemDao();
        this.mealTimesDao = new MealDao();
        this.mealTypesDao = new MealTypeDao();
        this.mealMenuDao = new MealItemDao();

        this.itemDao.createTable();
        this.mealTypesDao.createTable();

        this.mealTimesDao.createTable();
        this.mealMenuDao.createTable();
    }

    public TabularDto getMenuTable() {
        TabularDto dto = new TabularDto();
        dto.setBanner(ConsoleView.PLANNED_MENUS);
        dto.setTitle(ConsoleView.MENU_TITLE);

        List<String> options = new ArrayList<>();
        options.add("1. To Remove a Plan insert D[id]");
        options.add("2. To Update a Plan insert U[id] [DAY-MEAL_TIME-{item1,item2}]");
        options.add("3. To Add a Plan insert I[DAY-TYPE-HOUR-{item1,item2}]");
        options.add("4. Return Back to Home");
        options.add("0. Exit");
        dto.setOptions(options);

        dto.setCount(mealTimesDao.countAll());
        dto.setTableHead(String.format("%5s%10s%15s%10s%35s", "#id", "day", "type", "hour", "Item"));
        dto.setTableHead(dto.getTableHead() + "\n" + ConsoleView.SEPARATION_LINE);

        List<Meal> mealTimes = mealTimesDao.findAll();
        List<String> tableRows = new ArrayList<>();
        for (Meal meal : mealTimes) {
            tableRows.add(String.format("%5s%10s%15s%10s%35s", meal.getId(), meal.getWeekDay().getDay(),
                    meal.getMealTypesFk().getType(), meal.getMealTypesFk().getHour(), meal.itemsAsStringList()));
        }
        tableRows.add(ConsoleView.SEPARATION_LINE);
        dto.setTableRows(tableRows);

        return dto;
    }

    public void deleteMenu(int id) {
        Meal mealTimes = new Meal(id);
        if (mealTimesDao.doesExist(mealTimes)) {
            mealTimesDao.delete(mealTimes);
        }
    }

    public void insertMenu(String day, String type, String hour, String items) {

        String[] itemArray = items.split(",");
        List<Item> itemList = new ArrayList<>();
        for (String item : itemArray) {
            Item itemEntity = itemDao.findOneByName(item.trim());
            if (itemEntity == null) {
                itemEntity = itemDao.find(itemDao.insert(new Item(item)));
            }
            itemList.add(itemEntity);
        }

        WeekDay weekDay = WeekDay.valueOf(day);

        MealType mealTypes = mealTypesDao.findOneByNameHour(type, hour);
        if (mealTypes == null) {
            mealTypes = mealTypesDao.find(mealTypesDao.insert(new MealType(type, hour)));
        }

        Meal mealTimes = mealTimesDao.findByWeekDaysMealTypes(weekDay, mealTypes);

        if (mealTimes == null) {
            mealTimesDao.insert(new Meal(weekDay, mealTypes, itemList));

        } else {
            Meal newMealTimes = new Meal();
            newMealTimes.setMealTypesFk(mealTimes.getMealTypesFk());
            newMealTimes.setWeekDay(mealTimes.getWeekDay());
            newMealTimes.setItems(itemList);
            mealTimesDao.update(mealTimes, newMealTimes);
        }
    }

    public void updateMenu(int id, String day, String type, String hour, String items) {
        String[] itemArray = items.split(",");
        List<Item> itemList = new ArrayList<>();
        for (String item : itemArray) {
            Item itemEntity = itemDao.findOneByName(item.trim());
            if (itemEntity == null) {
                itemEntity = itemDao.find(itemDao.insert(new Item(item)));
            }
            itemList.add(itemEntity);
        }

        WeekDay weekDay = WeekDay.valueOf(day);

        MealType mealTypes = mealTypesDao.findOneByNameHour(type, hour);
        if (mealTypes == null) {
            mealTypes = mealTypesDao.find(mealTypesDao.insert(new MealType(type, hour)));
        }

        Meal oldMealTimes = mealTimesDao.find(id);
        if (oldMealTimes != null) {
            Meal newMealTimes = new Meal();
            newMealTimes.setMealTypesFk(mealTypes);
            newMealTimes.setWeekDay(weekDay);
            newMealTimes.setItems(itemList);
            mealTimesDao.update(oldMealTimes, newMealTimes);
        }
    }

    public static void main(String args[]) {
        MenuService menuService = new MenuService();
        menuService.insertMenu("Sunday", "Breakfast", "9:00", "Bread,Butter,Egg");
        //u[2][Monday-Lunch-13:00-{Chicken,Rice,Salad}]
    }
}

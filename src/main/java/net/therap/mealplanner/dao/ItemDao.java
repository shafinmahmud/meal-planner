package net.therap.mealplanner.dao;

import net.therap.mealplanner.annotation.CreateQuery;
import net.therap.mealplanner.controller.AppBootProcess;
import net.therap.mealplanner.db.DataSource;
import net.therap.mealplanner.db.GenericDao;
import net.therap.mealplanner.db.Table;
import net.therap.mealplanner.entity.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@CreateQuery(query = "CREATE TABLE IF NOT EXISTS `item` (" +
        "  `id` bigint(20) NOT NULL AUTO_INCREMENT," +
        "  `name` varchar(45) DEFAULT NULL," +
        "  PRIMARY KEY (`id`)," +
        "  UNIQUE (`name`)" +
        ") ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;")
@Table(name = "item")
public class ItemDao extends GenericDao<Item> {

    public ItemDao() {
        super(AppBootProcess.dataSource);
    }

    @Override
    public int insert(Item entity) {
        String query = "INSERT INTO item (name) VALUES(?)";
        return queryUtil.executeQuery(query, new Object[]{entity.getName()});
    }

    @Override
    public int update(Item oldEntity, Item newEntity) {
        String query = "UPDATE item SET name = ? WHERE id = ?";
        return queryUtil.executeQuery(query, new Object[]{newEntity.getName(), oldEntity.getId()});
    }

    @Override
    public int delete(Item entity) {
        String query = "DELETE FROM item WHERE id = ?";
        return queryUtil.executeQuery(query, new Object[]{entity.getId()});
    }

    @Override
    public int deleteAll() {
        String query = "DELETE FROM item";
        return queryUtil.executeQuery(query);
    }

    public Item getItemFromResultSet() {
        Item item = new Item();
        item.setId(queryUtil.getIntFromResultSet("id"));
        item.setName(queryUtil.getStringFromResultSet("name"));
        return item;
    }
}

package net.therap.mealplanner.dao;

import net.therap.mealplanner.annotation.CreateQuery;
import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import net.therap.mealplanner.entity.MealType;
import net.therap.mealplanner.entity.WeekDay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shafin
 * @since 11/13/16
 */
@CreateQuery(query = "CREATE TABLE IF NOT EXISTS `meal` (" +
        "  `id` bigint(20) NOT NULL AUTO_INCREMENT," +
        "  `day` ENUM ('SAT','SUN','MON','TUE','WED','THU','FRI') NULL," +
        "  `meal_type_id` bigint(20) DEFAULT NULL," +
        "  PRIMARY KEY (`id`)," +
        "  UNIQUE (`day`)," +
        "  FOREIGN KEY (`meal_type_id`) REFERENCES `meal_type` (`id`)" +
        ") ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;")
public class MealDao extends CommonDao<Meal> {

    private MealTypeDao mealTypesDao;
    private MealItemDao mealMenuDao;

    public MealDao() {
      //  super.queryUtil = new QueryUtil(new MySqlDBConn());
        this.mealTypesDao = new MealTypeDao();
        this.mealMenuDao = new MealItemDao();
    }

    @Override
    public boolean doesExist(Meal entity) {
        String query = "SELECT * FROM meal_time WHERE id = ?";
        queryUtil.retrieveResultSet(query, new Object[]{entity.getId()});
        return queryUtil.doesResultSetHasNext();
    }

    @Override
    public int insert(Meal entity) {
        String query = "INSERT INTO meal_time (week_day_fk, meal_type_fk) VALUES(?,?)";
        return queryUtil.executeQuery(query, new Object[]{entity.getWeekDay(),
                entity.getMealTypesFk().getId()});
    }

    @Override
    public Meal find(int id) {
        String query = "SELECT m.id,  t.id, w.id " +
                "FROM meal_time m, meal_type t, week_day w  " +
                "where m.meal_type_fk = t.id and m.week_day_fk = w.id and m.id = ?";
        queryUtil.retrieveResultSet(query, new Object[]{id});

        if (queryUtil.doesResultSetHasNext()) {
            return getMealTimeFromResultSet();
        }

        return null;
    }

    public Meal findByWeekDaysMealTypes(WeekDay day, MealType type) {
        String query = "SELECT m.id,  t.id, w.id " +
                "FROM meal_time m, meal_type t, week_day w  " +
                "where m.meal_type_fk = ? and m.week_day_fk = ?";
        queryUtil.retrieveResultSet(query, new Object[]{type.getId(), day});

        if (queryUtil.doesResultSetHasNext()) {
            return getMealTimeFromResultSet();
        }

        return null;
    }

    @Override
    public List<Meal> findAll() {
        String query = "SELECT m.id,  t.id, w.id " +
                "FROM meal_time m, meal_type t, week_day w  " +
                "where m.meal_type_fk = t.id and m.week_day_fk = w.id;";
        queryUtil.retrieveResultSet(query);

        List<Meal> list = new ArrayList<>();
        while (queryUtil.doesResultSetHasNext()) {
            list.add(getMealTimeFromResultSet());
        }
        return list;
    }

    @Override
    public int update(Meal oldEntity, Meal newEntity) {
        String query = "UPDATE meal_time SET week_day_fk = ?, meal_type_fk = ? WHERE id = ?";
        queryUtil.executeQuery(query, new Object[]{newEntity.getWeekDay(),
                newEntity.getMealTypesFk().getId(), oldEntity.getId()});

        if (newEntity.getItems() != null) {
            newEntity.setId(oldEntity.getId());

            Map<Meal, List<Item>> itemMap = new HashMap<>();
            itemMap.put(newEntity, newEntity.getItems());
            mealMenuDao.batchInsert(itemMap);
        }
        return 0;
    }

    @Override
    public int delete(Meal entity) {
        String query = "DELETE FROM meal_time WHERE id = ?";
        return queryUtil.executeQuery(query, new Object[]{entity.getId()});
    }

    @Override
    public int deleteAll() {
        String query = "DELETE FROM meal_time";
        return queryUtil.executeQuery(query);
    }

    @Override
    public int countAll() {
        String query = "SELECT count(*) as total FROM meal_time";
        queryUtil.retrieveResultSet(query);
        if (queryUtil.doesResultSetHasNext()) {
            return queryUtil.getIntFromResultSet("total");
        }
        return 0;
    }

    public Meal getMealTimeFromResultSet() {
        Meal mealTimes = new Meal();
        mealTimes.setId(queryUtil.getIntFromResultSet("m.id"));
        mealTimes.setMealTypesFk(mealTypesDao.find(queryUtil.getIntFromResultSet("t.id")));
       // mealTimes.setWeekDay(weekDaysDao.find(queryUtil.getIntFromResultSet("w.id")));
        mealTimes.setItems(mealMenuDao.findItemsByMealTimes(mealTimes.getId()));

        return mealTimes;
    }
}

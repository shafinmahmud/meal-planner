package net.therap.mealplanner.dao;

/**
 * @author shafin
 * @since 11/15/16
 */

import net.therap.mealplanner.annotation.CreateQuery;
import net.therap.mealplanner.entity.Item;
import net.therap.mealplanner.entity.Meal;
import net.therap.mealplanner.entity.MealItem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@CreateQuery(query = "CREATE TABLE IF NOT EXISTS `meal_item` (" +
        "  `meal_id` bigint(20) NOT NULL," +
        "  `item_id` bigint(20) NOT NULL," +
        "  FOREIGN KEY (`item_id`) REFERENCES `item` (`id`)," +
        "  FOREIGN KEY (`meal_id`) REFERENCES `meal` (`id`)" +
        ") ENGINE=InnoDB DEFAULT CHARSET=latin1;")
public class MealItemDao extends CommonDao<MealItem> {

    private ItemDao itemDao;

    public MealItemDao() {
       // super.queryUtil = new QueryUtil(new MySqlDBConn());
        this.itemDao = new ItemDao();
    }

    public void batchInsert(Map<Meal, List<Item>> menuMap) {

        String query = "DELETE FROM meal_menu WHERE meal_times_fk = ? ; " +
                "INSERT INTO meal_menu (meal_times_fk, items_fk) VALUES(?,?)";
        List<Object> params = new LinkedList<>();

        for (Map.Entry<Meal, List<Item>> entry : menuMap.entrySet()) {
            int timesId = entry.getKey().getId();
            params.add(timesId);

            for (Item item : entry.getValue()) {
                params.add(timesId);
                params.add(item.getId());
            }
        }
        queryUtil.executeQuery(query, params);
    }


    public List<Item> findItemsByMealTimes(int mealTimesFk) {
        String query = "SELECT item_fk FROM meal_menu where meal_time_fk = ?";
        queryUtil.retrieveResultSet(query, new Object[]{mealTimesFk});

        List<Item> itemList = new ArrayList<>();
        while (queryUtil.doesResultSetHasNext()) {
            itemList.add(itemDao.find(queryUtil.getIntFromResultSet("item_fk")));
        }

        return itemList;
    }

    @Override
    public boolean doesExist(MealItem entity) {
        return false;
    }

    @Override
    public int insert(MealItem entity) {
        return 0;
    }

    @Override
    public MealItem find(int id) {
        return null;
    }

    @Override
    public List<MealItem> findAll() {
        return null;
    }

    @Override
    public int update(MealItem oldEntity, MealItem newEntity) {
        return 0;
    }

    @Override
    public int delete(MealItem entity) {
        return 0;
    }

    @Override
    public int deleteAll() {
        return 0;
    }

    @Override
    public int countAll() {
        return 0;
    }
}
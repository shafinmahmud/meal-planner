package net.therap.mealplanner.dao;

import net.therap.mealplanner.annotation.CreateQueryProcessor;

import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
public abstract class CommonDao<T> {

    public QueryExecutor queryUtil;

    public void createTable() {
        String createQuery = CreateQueryProcessor.process(this);
        System.out.println(createQuery);
        queryUtil.executeQuery(createQuery);
    }

    public abstract boolean doesExist(T entity);

    public abstract int insert(T entity);

    public abstract T find(int id);

    public abstract List<T> findAll();

    public abstract int update(T oldEntity, T newEntity);

    public abstract int delete(T entity);

    public abstract int deleteAll();

    public abstract int countAll();
}

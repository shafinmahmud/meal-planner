package net.therap.mealplanner.dao;

import net.therap.mealplanner.annotation.CreateQuery;
import net.therap.mealplanner.entity.MealType;

import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
@CreateQuery(query = "CREATE TABLE IF NOT EXISTS `meal_type` (" +
        "  `id` bigint(20) NOT NULL AUTO_INCREMENT," +
        "  `hour` datetime DEFAULT NULL," +
        "  `type` varchar(45) DEFAULT NULL," +
        "  PRIMARY KEY (`id`)" +
        ") ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;")
public class MealTypeDao extends CommonDao<MealType> {

    public MealTypeDao() {
      //  super.queryUtil = new QueryUtil(new MySqlDBConn());
    }

    @Override
    public boolean doesExist(MealType entity) {
        String query = "SELECT * FROM meal_type WHERE id = ?";
        queryUtil.retrieveResultSet(query, new Object[]{entity.getId()});
        return queryUtil.doesResultSetHasNext();
    }

    @Override
    public int insert(MealType entity) {
        String query = "INSERT INTO meal_type (type, hour) VALUES(?,?)";
        return queryUtil.executeQuery(query, new String[]{entity.getType(), entity.getHour()});
    }

    @Override
    public MealType find(int id) {
        String query = "SELECT * FROM meal_type where id = ?";
        queryUtil.retrieveResultSet(query, new Object[]{id});
        if (queryUtil.doesResultSetHasNext()) {
            getMealTypeFromResultSet();
        }

        return null;
    }

    public MealType findOneByNameHour(String type, String hour) {
        String query = "SELECT * FROM meal_type where type = ? and hour = ?";
        queryUtil.retrieveResultSet(query, new Object[]{type, hour});
        if (queryUtil.doesResultSetHasNext()) {
            return getMealTypeFromResultSet();
        }
        return null;
    }

    @Override
    public List<MealType> findAll() {
        return null;
    }

    @Override
    public int update(MealType oldEntity, MealType newEntity) {
        return 0;
    }

    @Override
    public int delete(MealType entity) {
        return 0;
    }

    @Override
    public int deleteAll() {
        return 0;
    }

    @Override
    public int countAll() {
        String query = "SELECT count(*) as total FROM meal_type";
        queryUtil.retrieveResultSet(query);
        if (queryUtil.doesResultSetHasNext()) {
            queryUtil.getIntFromResultSet("total");
        }
        return 0;
    }

    public MealType getMealTypeFromResultSet() {
        if (queryUtil.doesResultSetHasNext()) {
            MealType type = new MealType();
            type.setId(queryUtil.getIntFromResultSet("id"));
            type.setType(queryUtil.getStringFromResultSet("type"));
            type.setHour(queryUtil.getStringFromResultSet("hour"));
            return type;
        }
        return null;
    }
}

package net.therap.mealplanner.db;

import java.sql.ResultSet;

/**
 * @author shafin
 * @since 5/21/17
 */
public interface RowMapper<T> {

    T mapRow(ResultSet resultSet);
}

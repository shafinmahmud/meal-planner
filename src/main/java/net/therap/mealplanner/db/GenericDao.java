package net.therap.mealplanner.db;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author shafin
 * @since 5/21/17
 */
public abstract class GenericDao<T extends Serializable> {

    private String tableName;

    public DataSource dataSource;
    public QueryTemplate template;

    public GenericDao(DataSource dataSource) {
        this.dataSource = dataSource;
        this.template = new QueryTemplate(dataSource.getConnection());

        Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];

        this.tableName = clazz.getAnnotation(Table.class).name();
    }

    public boolean isExist(long id) {
        String query = "SELECT id FROM " + tableName + " WHERE id = ?";
        return this.template.queryForExistence(query, new Object[]{id});
    }

    public boolean isExist(String whereField, Object fieldValue) {
        String query = "SELECT * FROM " + tableName + " WHERE " + whereField + " = ?";
        return this.template.queryForExistence(query, new Object[]{fieldValue});
    }

    public T find(long id, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " WHERE id = ?";
        List<T> list = this.template.retrieveData(query, mapper, new Object[]{id});

        return getFirstItem(list);
    }

    public T find(String whereField, Object fieldValue, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " WHERE " + whereField + " = ?";
        List<T> list = this.template.retrieveData(query, mapper, new Object[]{fieldValue});

        return getFirstItem(list);
    }

    public T find(String whereField1, Object fieldValue1,
                  String whereField2, Object fieldValue2, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " WHERE " + whereField1 + " = ? and " + whereField2 + " = ?";
        List<T> list = this.template.retrieveData(query, mapper, new Object[]{fieldValue1, fieldValue2});

        return getFirstItem(list);
    }

    public List<T> findAll(RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName;
        return this.template.retrieveData(query, mapper);
    }

    public List<T> findAll(String whereField, Object fieldValue, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " WHERE " + whereField + " = ?";
        return this.template.retrieveData(query, mapper, new Object[]{fieldValue});
    }

    public List<T> findAllBy(String whereField1, Object fieldValue1,
                             String whereField2, Object fieldValue2, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " WHERE " + whereField1 + " = ? and " + whereField2 + " = ?";
        return this.template.retrieveData(query, mapper, new Object[]{fieldValue1, fieldValue2});
    }

    public List<T> findAll(int page, int size, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " LIMIT ? OFFSET ?";
        return this.template.retrieveData(query, mapper, new Object[]{size, (page - 1) * size});
    }

    public List<T> findAll(String whereField, Object fieldValue, int page, int size, RowMapper<T> mapper) {
        String query = "SELECT * FROM " + tableName + " WHERE " + whereField + "=? LIMIT = ? and OFFSET = ?";
        return this.template.retrieveData(query, mapper, new Object[]{fieldValue, size, (page - 1) * size});
    }

    public List<T> findAllSortedBy(String whereField, Object fieldValue) {
        return null;
    }

    public T findByFuzzyValue(String whereField, String fuzzyValue) {
        return null;
    }

    public List<T> findAllByFuzzyValue(String whereField, String fuzzyValue) {
        return null;
    }

    public Long countRowsAll() {
        String query = "SELECT COUNT(id) FROM " + tableName;
        return this.template.countRows(query);
    }

    public Long countRowsAll(String whereField, String fieldValue) {
        String query = "SELECT COUNT(id) FROM " + tableName + " WHERE " + whereField + "=?";
        return this.template.countRows(query, new Object[]{fieldValue});
    }


    public T save(T entity) {
        if (entity != null) {
            //em.persist(entity);
        }

        return entity;
    }

    public T update(T entity) {
//        return em.merge(entity);
    }

    public T updateFields(String whereField, Object whereValue, T updatedT, String... ignoreFields) {
//        T oldT = findBy(whereField, whereValue);
//        oldT = ReflectionUtil.updateBeanFields(clazz, oldT, updatedT, ignoreFields);
//
//        em.persist(oldT);
//
//        return oldT;
    }
//
    public void delete(long entityId) {
//        T entity = find(entityId);
//        delete(entity);
    }
//
   public void deleteBy(String whereField, Object fieldValue) {
//        em.createQuery("DELETE FROM " + clazz.getName() + " WHERE " + whereField + " = :val")
//                .setParameter("val", fieldValue)
//                .executeUpdate();
    }

    public T getFirstItem(List<T> list) {
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }
}

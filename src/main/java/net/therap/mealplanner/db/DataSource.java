package net.therap.mealplanner.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author shafin
 * @since 11/13/16
 */
public class DataSource {

    public static Connection connection;

    protected String connectionUrl;
    protected String user;
    protected String password;

    public DataSource(ConnectionConfig config) {
        this.user = config.user;
        this.password = config.password;
        this.connectionUrl = config.connectionUrl;
    }

    public boolean isConnected() {
        return connection != null;
    }

    public Connection getConnection() {
        return isConnected() ? connection : initConnection();
    }

    private Connection initConnection() {
        try {
            return authNotRequired() ? DriverManager.getConnection(connectionUrl)
                    : DriverManager.getConnection(connectionUrl, user, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private boolean authNotRequired() {
        return user == null || user.isEmpty();
    }

    public static class ConnectionConfig {
        private String connectionUrl;
        private String user;
        private String password;

        public ConnectionConfig(String connectionUrl) {
            this.connectionUrl = connectionUrl;
        }

        public ConnectionConfig setUser(String user) {
            this.user = user;
            return this;
        }

        public ConnectionConfig setPassword(String password) {
            this.password = password;
            return this;
        }
    }
}

package net.therap.mealplanner.controller;

import net.therap.mealplanner.dto.WelcomeDto;

import java.util.Scanner;

/**
 * @author shafin
 * @since 11/14/16
 */
public class WelcomeViewController {

    private Scanner scanner;

    public WelcomeViewController() {
        this.scanner = new Scanner(System.in);
    }

    public void view() {
        ConsoleView.viewHome(new WelcomeDto());

        switch (scanner.next()) {
            case "1":
                ItemViewController itemView = new ItemViewController();
                itemView.view();
                break;

            case "2":
                MenuViewController menuView = new MenuViewController();
                menuView.view();
                break;

            case "0":
                ConsoleView.exit();
                break;

            default:
                System.out.println("Invalid Input.");
                break;
        }
    }
}

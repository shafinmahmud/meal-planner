package net.therap.mealplanner.controller;

import net.therap.mealplanner.db.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author shafin
 * @since 11/21/17
 */
public class AppBootProcess {

    public static DataSource dataSource;

    public static void initDataEnvironment() {
        try {
            Properties dbProperties = readPropertyFile("db.properties");
            String url = dbProperties.getProperty("jdbc.url");
            String user = dbProperties.getProperty("jdbc.user");
            String pass = dbProperties.getProperty("jdbc.pass");

            DataSource.ConnectionConfig dbConfig = new DataSource.ConnectionConfig(url);
            dbConfig.setUser(user);
            dbConfig.setPassword(pass);

            dataSource = new DataSource(dbConfig);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Properties readPropertyFile(String propertyFileName) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties props = new Properties();

        try(InputStream resourceStream = loader.getResourceAsStream(propertyFileName)) {
            props.load(resourceStream);
        }

        return props;
    }
}

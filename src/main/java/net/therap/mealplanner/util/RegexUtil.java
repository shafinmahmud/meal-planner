package net.therap.mealplanner.util;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author shafin
 * @since 11/14/16
 */
public class RegexUtil {

    public static String getAnyMatched(String text, String regex, int group) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(group);
        }
        return null;
    }

    public static void main(String[] args) {
        /*String regex = "URI=\\[(.*)\\]";
        String text = "2013-08-07 11:24:06,784 [[ACTIVE] ExecuteThread: '0' for queue: 'weblogic.kernel.Default "
                + "(self-tuning)'] U:osl@OSL-NM A:fbd0e570 [PROFILER:132] DEBUG - URI=[/ma/entry], G, time=9ms";

        System.out.println(getAnyMatched(text, regex,1));*/

        Object[] array = new Object[]{"Hello", 1, 2};
        for (Object ob : array) {
            System.out.println(ob.getClass());
        }
    }

}

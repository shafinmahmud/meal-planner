package net.therap.mealplanner.entity;

import java.io.Serializable;

/**
 * @author shafin
 * @since 11/13/16
 */
public class Item implements Serializable{

    private int id;
    private String name;

    public Item() {
    }

    public Item(int id) {
        this.id = id;
    }

    public Item(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

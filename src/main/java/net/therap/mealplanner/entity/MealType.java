package net.therap.mealplanner.entity;

/**
 * @author shafin
 * @since 11/13/16
 */
public class MealType {

    private int id;
    private String type;
    private String hour;

    public MealType() {
    }

    public MealType(String type, String hour) {
        this.type = type;
        this.hour = hour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }
}

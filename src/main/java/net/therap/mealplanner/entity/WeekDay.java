package net.therap.mealplanner.entity;

/**
 * @author shafin
 * @since 5/21/17
 */
public enum WeekDay {

    SAT(1, "Saturday"),
    SUN(2, "Sunday"),
    MON(3, "Monday"),
    TUE(4, "Tuesday"),
    WED(5, "Wednesday"),
    THU(6, "Thursday"),
    FRI(7, "Friday");

    private int number;
    private String day;

    WeekDay(int number, String day) {
        this.number = number;
        this.day = day;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}

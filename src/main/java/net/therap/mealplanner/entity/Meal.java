package net.therap.mealplanner.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/13/16
 */
public class Meal {

    private int id;
    private WeekDay weekDay;
    private MealType mealTypesFk;
    private List<Item> items;

    public Meal() {
    }

    public Meal(int id) {
        this.id = id;
    }

    public Meal(WeekDay weekDay, MealType mealTypesFk, List<Item> itemList) {
        this.weekDay = weekDay;
        this.mealTypesFk = mealTypesFk;
        this.items = itemList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
    }

    public MealType getMealTypesFk() {
        return mealTypesFk;
    }

    public void setMealTypesFk(MealType mealTypesFk) {
        this.mealTypesFk = mealTypesFk;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<String> itemsAsStringList(){
        List<String> itemList = new ArrayList<>();
        for(Item item : this.items){
            itemList.add(item.getName());
        }
        return itemList;
    }
}

package net.therap.mealplanner.entity;

import java.util.List;

/**
 * @author shafin
 * @since 11/16/16
 */
public class MealItem {

    private List<Meal> mealTimes;
    private List<Item> items;

    public MealItem() {
    }

    public List<Meal> getMealTimes() {
        return mealTimes;
    }

    public void setMealTimes(List<Meal> mealTimes) {
        this.mealTimes = mealTimes;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}

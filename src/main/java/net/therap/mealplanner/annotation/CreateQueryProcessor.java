package net.therap.mealplanner.annotation;

/**
 * @author shafin
 * @since 11/13/16
 */
public class CreateQueryProcessor {

    public static String process(Object entity) {
        CreateQuery queryAnnotation = entity.getClass().getAnnotation(CreateQuery.class);
        return queryAnnotation.query();
    }
}
